package com.example.app.ui.food;


import android.widget.TextView;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.example.app.BR;

public class AddFoodFields extends BaseObservable {
    private String foodName;
    private int calories;

    @Bindable
    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        if(this.foodName == null){
            this.foodName = foodName;
        }

        if (!this.foodName.equals(foodName)) {
            this.foodName = foodName;
            notifyPropertyChanged(BR.foodName);
        }
    }

    @Bindable
    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        if(this.calories != calories){
            this.calories = calories;
            notifyPropertyChanged(BR.calories);
        }


    }
}
