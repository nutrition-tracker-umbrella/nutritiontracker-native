package com.example.app.ui.food;

import androidx.lifecycle.ViewModel;

public class AddFoodViewModel extends ViewModel {
    private final AddFoodFields addFoodFields = new AddFoodFields();

    public AddFoodFields getAddFoodFields() {
        return addFoodFields;
    }
}
