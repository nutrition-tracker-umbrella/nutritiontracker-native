package  com.example.app.ui.food;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.app.R;
import com.example.app.databinding.FragmentAddFoodBinding;

public class AddFoodFragment extends Fragment implements View.OnClickListener {


    public static AddFoodFragment newInstance() {
        return new AddFoodFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_food, container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        AddFoodViewModel viewModel = new ViewModelProvider(this).get(AddFoodViewModel.class);
        Button b = view.findViewById(R.id.create_food);

        FragmentAddFoodBinding viewDataBinding = DataBindingUtil.setContentView(this.getActivity(), R.layout.fragment_add_food);
      //  viewDataBinding.setModel(viewModel);

        view.setClickable(false);


        System.out.println("TEESST 1");

        if(b == null){
            System.out.println("b is null");
        }
        if(b != null){
            System.out.println("b is not null");
        }

        b.setOnClickListener(this);

        System.out.println(b);

        System.out.println("TEESST 2");

    }


    @Override
    public void onClick(View v) {
        System.out.println("Inside on click");

        switch (v.getId()){
            case (R.id.create_food) :
            {
                NavDirections actions = AddFoodFragmentDirections.actionAddFoodFragmentToMainFragment();
                Navigation.findNavController(v).navigate(actions);
            }

        }
    }
}