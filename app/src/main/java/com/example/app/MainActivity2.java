package com.example.app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import android.os.Bundle;
import android.view.View;

import com.example.app.ui.main.MainFragment;
import com.example.app.ui.main.MainFragmentDirections;

import java.util.HashMap;
import java.util.Map;

public class MainActivity2 extends AppCompatActivity {

    private final NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.fragment3);

    public final static Map<Integer, Object> db = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .show(MainFragment.newInstance())
                    .commitNow();
        }
    }

    public void addFood(View view){
        NavDirections actions = MainFragmentDirections.actionMainFragmentToAddFoodFragment();
        Navigation.findNavController(view).navigate(actions);
    }





}